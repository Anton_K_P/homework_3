package app;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Введите имя");
        Scanner input = new Scanner(System.in);
        String name = input.next();
        System.out.println("Введите возраст");
        int age = input.nextInt();
        System.out.println("Введите размер желаемой суммы кредита");
        int sum = input.nextInt();
        String message = (age >= 18 && sum <= age * 100  && !name.equals("Bob"))  ? "Кредит выдан" : "Кредит не выдан";
        System.out.println(message);
    }

}
